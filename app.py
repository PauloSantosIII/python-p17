from flask import Flask, request
from handlers import *
import os
import csv

app = Flask(__name__)

@app.route('/signup', methods=['POST', 'GET'])
def signup():
    name = request.json.get('name')
    email = request.json.get('email')
    password = request.json.get('password')
    age = request.json.get('age')

    register(name, email, password, age)

    return dict(name=name, email=email, age=age)
    
@app.route('/login', methods=['POST', 'GET'])
def login():
    email = request.json.get('email')
    password = request.json.get('password')

    return log_user_in(email, password)