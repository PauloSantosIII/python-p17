import csv
import os

filename = 'register_list.csv'

def register(name, email, password, age):
    test = []
    with open(filename, 'r') as read:
        for row in csv.reader(read):
            test.append(row)
            if row[0] == str(name):
                return 'Nome de usuário já extiste'

    with open(filename, 'a+') as f:

        fieldnames = ['name', 'email', 'password', 'age']
        writer = csv.DictWriter(f, fieldnames=fieldnames)

        if os.stat('register_list.csv').st_size == 0:
            writer.writeheader()

        

        writer.writerow({
            'name': name,
            'email': email,
            'password': password,
            'age': age
        })


def log_user_in(email, password):
    with open(filename, 'r') as f:
        log_data = csv.DictReader(f)
        for user in log_data:
            if user['email'] == email and user['password'] == password:
                return {
                    'name': user['name'],
                    'email': user['email'],
                    'age': user['age']
                }
        
        return {}